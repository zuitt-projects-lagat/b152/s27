let http = require("http");

//Mock Data for Users
let users =[

	{
		username: "moonknight1999",
		email: "moonGod@gmail.com",
		password: "moonKnightStrong"
	},
	{
		username: "kitkatMachine",
		email: "notSponsored@gmail.com",
		password: "kitkatForever"
	}

];

let courses = [

	{
		name: "Science 101",
		price: 2500,
		isActive: true
	},
	{
		name: "English 101",
		price: 2500,
		isActive: true
	}

];


http.createServer((req,res)=>{

	console.log(req.url);// request URL endpoint
	console.log(req.method);//request method

	/*
		Not only can we get the request url endpoint to differentiate requests but we can also check for the request method.

		We can think of request methods as the request/client telling our server the action to take for their request.

		This way we don't need to have different endpoints for everything that our client wants to do.

		HTTP methods allow us to group and organize our routes.

		With this we can have the same endpoints but with different methods.

		HTTP methods are particularly and primarily concerned with CRUD operations.

		Common HTTP Methods:

		GET - Get method for request indicates that the client/request wants to retrieve or get data.

		POST - Post method for request indicates that the client/request wants to post data and create a document.

		PUT - Put method for request indicates that the client/request wants to input data and update a document.

		DELETE - Delete method for request indicates that the client/request wants to delete a document.

	*/

	if(req.url === "/" && req.method === "GET"){

		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Hello World! This route checks for GET Method.");

	} else if(req.url === "/" && req.method === "POST"){

		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Hello World! This route checks for POST Method.");

	} else if(req.url === "/" && req.method === "PUT"){

		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Hello World! This route checks for PUT Method.");

	} else if(req.url === "/" && req.method === "DELETE"){

		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Hello World! This route checks for DELETE Method.");

	} else if(req.url === "/users" && req.method === "GET"){

		//Change the value of Content-Type header if were passing json as our server response: 'application/json'
		res.writeHead(200, {'Content-Type': 'application/json'});
		//We cannot pass other data type as response except for strings.
		//To be able to pass the array of users, first we stringify the array as JSON
		res.end(JSON.stringify(users));

	} else if(req.url === "/users" && req.method === "POST"){

		//This route should allow us to receive data input from our client and add that data as a new user object in our users array.

		//This will act as a placeholder for the body of our POST request.
		let requestBody = "";

		//Receiving data from a client to a nodejs server requires 2 steps:

		//data step - this part wll read the stream of data from our client and process the incoming data into the requestBody variable
		req.on('data', function(data){

			//console.log(data);
			requestBody += data;

		})

		//end step - this will run once or after the request has been completely sent from our client.
		req.on('end',function(){

			//requestBody now contains the data from our Postman client
			//However it is in JSON format. To process our request body we have to parse it into a proper JS Object using JSON.parse
			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			//Simulate creating a document and adding into a collection
			let newUser = {

				username: requestBody.username,
				email: requestBody.email,
				password: requestBody.password

			}

			users.push(newUser);
			console.log(users);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.end(JSON.stringify(users));

		})

	} else if(req.url === "/courses" && req.method === "GET"){

		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(courses));

	} else if(req.url === "/courses" && req.method === "POST"){

		let requestBody = "";

		req.on('data', function(data){

			//console.log(data);
			requestBody += data;

		})

		req.on('end',function(){

			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			//simulate creating a new course document to be pushed into our courses array/collection
			let newCourse = {

				name: requestBody.name,
				price: requestBody.price,
				isActive: requestBody.isActive

			}

			courses.push(newCourse);
			console.log(courses);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.end(JSON.stringify(courses));

		})

	}

}).listen(4000);

console.log('Server is running on localhost:4000');
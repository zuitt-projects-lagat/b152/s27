let http = require("http");

//Mock Data
let items = [

	{
		name: "Iphone X",
		price: 30000,
		isActive: true
	},
	{
		name: "Samsung Galaxy S21",
		price: 51000,
		isActive: true
	},
	{
		name: "Razer BlackShark VX2",
		price: 2800,
		isActive: false
	}

];



http.createServer((req,res)=>{

	if (req.url === "/items" && req.method === "GET"){

		// 1st Route

		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(items));

	} else if (req.url === "/items" && req.method === "POST"){

		// 2nd Route

		let requestBody = "";

		req.on('data', function(data){

			requestBody += data;

		})

		req.on('end', function(){

			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			let newItem = {

				name: requestBody.name,
				price: requestBody.price,
				isActive: requestBody.isActive

			}

			items.push(newItem);
			console.log(items);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.end(JSON.stringify(items));

		})

	} else if (req.url === "/items" && req.method === "DELETE"){

		// Additional Route

		let requestBody = "";

		req.on('data', function(){

			requestBody += data;

		})

		req.on('end', function(){

			items.pop();
			console.log(items);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.end(JSON.stringify(items));

		})

	} else {

		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.end('Resource cannot be found.');

	}

}).listen(8000);

console.log('Server is running on localhost:8000');